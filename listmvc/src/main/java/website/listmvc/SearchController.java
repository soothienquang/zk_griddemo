package website.listmvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.ForwardEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Image;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.ext.Selectable;



public class SearchController  extends SelectorComposer<Component>{
	  private static final long serialVersionUID = 1L;
	     
	    @Wire
	    private Textbox keywordBox,id;
	    @Wire
	    private Listbox carListbox;
	    @Wire
	    private Textbox modelLabel;
	    @Wire
	    private Textbox makeLabel;
	    @Wire
	    private Textbox priceLabel;
	    @Wire
	    private Textbox descriptionLabel;
	    @Wire
	    private Image previewImage;
	    @Wire
	    private Component detailBox;
	     ListModelList<Car> sanpham =new ListModelList<Car>(new CarServiceImpl().getfindAll());
	  
	    public ListModelList<Car> getSanphams() {
	    	return sanpham;
		}
	    
	
	     
	    @Listen("onClick = #searchButton")
	    public void search(){
	        String keyword = keywordBox.getValue();
	        List<Car> result = new ArrayList<Car>(new CarServiceImpl().getsearch(keyword));
	        carListbox.setModel(new ListModelList<Car>(result));
	    }
	    CarServiceImpl cr=new CarServiceImpl();
	    @Listen("onClick = #add")
	    public void add(){
	    	Car c=new Car(Integer.parseInt(id.getText()),modelLabel.getText(),makeLabel.getText(),descriptionLabel.getText(),"",Integer.parseInt(priceLabel.getText()));
	      if(cr.kiemTraTrungMa(c.getId())==false)
	      {
	    	  Messagebox.show("Trung ma");
	    	  return;
	      }
	      else
	      {
	    		sanpham.add(c);
	    		Messagebox.show("Thanh cong");
	      }
	    
	      
	    }
	    @Listen("onSelect = #carListbox")
	    public void showDetail(){
	        detailBox.setVisible(true);
	         
	        Set<Car> selection = ((Selectable<Car>)carListbox.getModel()).getSelection();
	        if (selection!=null && !selection.isEmpty()){
	            Car selected = selection.iterator().next();
	            id.setValue(selected.getId().toString());
	            previewImage.setSrc(selected.getPreview());
	            modelLabel.setValue(selected.getModel());
	            makeLabel.setValue(selected.getMake());
	            priceLabel.setValue(selected.getPrice().toString());
	            descriptionLabel.setValue(selected.getDescription());
	        }
	    }
	    
	    
	    @Listen("onDeleteKH = #carListbox")
		public void DeleteKH(ForwardEvent evt){
			Button btn = (Button)evt.getOrigin().getTarget();
			Listitem litem = (Listitem)btn.getParent().getParent();
			
			Car kh = (Car)litem.getValue();
			sanpham.remove(kh);
		}
	    
	    @Override
		public void doAfterCompose(Component comp) throws Exception{
			super.doAfterCompose(comp);
			
			//get data from service and wrap it to list-model for the view
			//ListModelList<Car> sanpham =new ListModelList<Car>(new CarServiceImpl().getfindAll());
			  carListbox.setModel(sanpham);
		}
	    
	    
	    @Listen("onClick = #btnsave")
		public void saveKH() {
			for (Car kh : sanpham) {
				if(kh.getId()==Integer.parseInt(id.getText()))
				{
					Car c=new Car(Integer.parseInt(id.getText()),modelLabel.getText(),makeLabel.getText(),descriptionLabel.getText(),"",Integer.parseInt(priceLabel.getText()));
					sanpham.set(sanpham.indexOf(kh), c);
					sanpham.addToSelection(c);
				}	
			}
			//show message for user
		Messagebox.show("Save successfully");
		}

}
