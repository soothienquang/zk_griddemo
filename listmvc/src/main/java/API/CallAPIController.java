package API;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Messagebox;

import website.listmvc.Car;
import website.listmvc.CarServiceImpl;

public class CallAPIController extends SelectorComposer<Component> {
	@Wire
	Combobox cbbtinh,cbbhuyen,cbbxa;
	JSONArray json;
	JSONArray huyen;
	public ListModelList<String> getTinh() {
		List<String> lsttinh = new ArrayList<String>();
		String strJson = getJSONFromURL("https://provinces.open-api.vn/api/?depth=3");
		try {
			json = new JSONArray(strJson);
			for (int i = 0; i < json.length(); i++) {
				JSONObject tinh = (JSONObject) json.get(i);
				String type = (String) tinh.get("name");
				lsttinh.add(type);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return new ListModelList<String>(lsttinh);
	
	}
//vieets su kien khi chon combobox tinh thanh pho
	  @Listen("onChange = #cbbtinh")
	    public void changeCountry() {
	        String tinh = cbbtinh.getValue();
	       cbbhuyen.setModel(new ListModelList<String>(locHuyenTheoTP(tinh)));
	       cbbhuyen.setText("--Chọn Quận/Huyện--");
	       cbbxa.setText("--Chọn Xã/ Phường--");
	    }
	  @Listen("onChange = #cbbhuyen")
	    public void changexa() {
		  String huyen = cbbhuyen.getValue();
	       cbbxa.setModel(new ListModelList<String>(locxaTheoHuyen(huyen)));
	       cbbxa.setText("--Chọn Xã/ Phường--");
	    }
	public List<String>locHuyenTheoTP(String tentp)
	{
		List<String> lsthuyen = new ArrayList<String>();
		 try {
				for (int i = 0; i < json.length(); i++) {
					JSONObject tinh = (JSONObject) json.get(i);
					String type = (String) tinh.get("name");
					if (type.equals(tentp)) {
						huyen=(JSONArray)tinh.get("districts");
						for (int j = 0; j < huyen.length(); j++) {
							JSONObject q = (JSONObject) huyen.get(j);
							String type1 = (String) q.get("name");
							lsthuyen.add(type1);
						}
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		 return lsthuyen;
	}
	public List<String>locxaTheoHuyen(String tenhuyen)
	{
		List<String> lsthuyen = new ArrayList<String>();
		 try {
				for (int i = 0; i < huyen.length(); i++) {
					JSONObject xa = (JSONObject) huyen.get(i);
					String type = (String) xa.get("name");
					if (type.equals(tenhuyen)) {
						huyen=(JSONArray)xa.get("wards");
						for (int j = 0; j < huyen.length(); j++) {
							JSONObject q = (JSONObject) huyen.get(j);
							String type1 = (String) q.get("name");
							lsthuyen.add(type1);
						}
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		 return lsthuyen;
	}
	
	
	public static String getJSONFromURL(String strUrl) {
		String jsonText = "";
		StringBuilder str = new StringBuilder();
		try {
			URL url = new URL(strUrl);
			InputStream is = url.openStream();

			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));

			String line;
			while ((line = bufferedReader.readLine()) != null) {
				// jsonText += line + "\n";
				str.append(line + "\n");
			}

			is.close();
			bufferedReader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return str.toString();
	}
}
